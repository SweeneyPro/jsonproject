﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class DocumentConditions
{
    // Example of finding strings
    public static bool ScoreBoardKickOffIs(JSONDocument document, int value)
    {
        return document.GetDocument()["Tool"]["Scoreboard"]["Kickoff"].ToString() == value.ToString();
    }

    // Example of Finding and Converting Bools
    public static bool ScoreBoardKOIs(JSONDocument document, bool value)
    {
        return Convert.ToBoolean(document.GetDocument()["Tool"]["Scoreboard"]["KO"].ToString()) == value;
    }

    // Example of Dynamically Finding Sections in an Array
    public static bool ScoreBoardTeamGoalsIs(JSONDocument document, int teamIndex, int score)
    {
        return document.GetDocument()["Tool"]["Scoreboard"]["Teams"][$"Team{teamIndex}"]["Goals"].ToString() == score.ToString();
    }

    // Example of Checking if a Section/Object Exists
    public static bool ScoreBoardExists(JSONDocument document)
    {
        return document.GetDocument()["Tool"]["Scoreboard"] != null;
    }

}

